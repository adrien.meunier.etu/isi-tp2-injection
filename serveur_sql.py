#!/usr/bin/env python3

import mysql.connector
import cherrypy
import config

class VulnerableApp(object):
    def __init__(self):
        self.conn = mysql.connector.connect(host=config.DB_HOST, user=config.DB_USER, database=config.DB_NAME, password=config.DB_PASS)

    @cherrypy.expose
    def index(self, **post):
        try:
            cursor = self.conn.cursor(prepared=True)
            if cherrypy.request.method == "POST":
                requete = """INSERT INTO chaines (txt) VALUES(%s);"""
                print(requete,post["chaine"])
                cursor.execute(requete,(post["chaine"],))
                self.conn.commit()
        except mysql.connector.Error as error:
            print("parameterized query failed {}".format(error))
        finally:
            chaines = []
            cursor.execute("SELECT txt FROM chaines")
            chaines = [row[0] for row in cursor.fetchall()]
            cursor.close()
            return '''
<html>
<head>
<title>Application Python Vulnerable</title>
</head>
<body>
<p>
Bonjour, je suis une application vulnerable qui sert a inserer des chaines dans une base de données MySQL!
</p>

<p>
Liste des chaines actuellement insérées:
<ul>
'''+"\n".join(["<li>" + s + "</li>" for s in chaines])+'''
</ul>
</p>

<p> Inserer une chaine:

<form method="post" onsubmit="return validate()">
<input type="text" name="chaine" id="chaine" value="" />
<br />
<input type="submit" name="submit" value="OK" />
</form>

<script>
function validate() {
    var regex = /^[a-zA-Z0-9]+$/;
    var chaine = document.getElementById('chaine').value;
    console.log(regex.test(chaine));
    if (!regex.test(chaine)) {
        alert("Veuillez entrer une chaine avec uniquement des lettres et des chiffres");
        return false;
    }
    return true;
}
</script>

</p>
</body>
</html>
'''


cherrypy.quickstart(VulnerableApp())

